#include <gtk/gtk.h>
#include <liblsdj/song.h>
#include <liblsdj/channel.h>
#include <liblsdj/row.h>
#include "song_screen.h"
#include "chain_screen.h"
#include "print_byte.h"
#include "model.h"
#include "row_number_widget.h"

static void song_row_button_callback(GtkWidget *widget, gpointer data) {
	model_t *model = (model_t *)data;
	if(GTK_IS_BIN(widget)) {
		GValue gvalue_row = G_VALUE_INIT;
		g_value_init(&gvalue_row, G_TYPE_INT);
		gtk_container_child_get_property(GTK_CONTAINER(get_song_screen(model)), widget, "top-attach", &gvalue_row);
		gint row = g_value_get_int(&gvalue_row);

		for(int channel = 0; channel < LSDJ_CHANNEL_COUNT; channel++) {
			GtkWidget *chain_widget = gtk_grid_get_child_at(GTK_GRID(get_song_screen(model)), channel + 1, row);
			gint64 chain_id = g_ascii_strtoll(gtk_entry_get_text(GTK_ENTRY(chain_widget)), NULL, 16);

			do_command(model, UPDATE_CHAIN_SCREEN, chain_id, channel, 0);
		}
	}
}

static void chain_widget_callback(GtkWidget *chain_widget, GdkEvent *event, gpointer data) {
	model_t *model = (model_t *)data;
	gint64 chain_id = g_ascii_strtoll(gtk_entry_get_text(GTK_ENTRY(chain_widget)), NULL, 16);

	GValue gvalue_column = G_VALUE_INIT;
	g_value_init(&gvalue_column, G_TYPE_INT);
	gtk_container_child_get_property(GTK_CONTAINER(get_song_screen(model)), chain_widget, "left-attach", &gvalue_column);
	gint column = g_value_get_int(&gvalue_column);

	do_command(model, UPDATE_CHAIN_SCREEN, chain_id, column-1, 0);
}

void song_screen_update(GtkWidget *song_grid, lsdj_song_t *song) {
	gchar label[32] = "00";
	for (size_t row_index = 0; row_index < 0x10; row_index++) {
		lsdj_row_t *row = lsdj_song_get_row(song, row_index);

		for(size_t channel = 0; channel < LSDJ_CHANNEL_COUNT; channel++) {
			snprint_byte(label, sizeof(label), row->channels[channel]);
			GtkWidget *chain_widget = gtk_grid_get_child_at(GTK_GRID(song_grid), channel + 1, row_index);
			gtk_entry_set_text(GTK_ENTRY(chain_widget), label);
		}
	}
}

GtkWidget *create_chain_widget(model_t *model) {
	GtkWidget *chain_widget = gtk_entry_new();
	gtk_entry_set_max_width_chars(GTK_ENTRY(chain_widget), 2);
	gtk_entry_set_text(GTK_ENTRY(chain_widget), "--");
	gtk_widget_set_vexpand(chain_widget, TRUE);
	g_signal_connect(chain_widget, "focus-in-event", G_CALLBACK(chain_widget_callback), model);
	return chain_widget;
}

GtkWidget *song_screen_new(model_t *model) {
	GtkWidget *grid = gtk_grid_new();

	gtk_grid_set_column_homogeneous(grid, TRUE);

	for (int row = 0; row < 0x10; row++) {
		gtk_grid_attach(GTK_GRID(grid), create_row_number_widget(model, row, G_CALLBACK(&song_row_button_callback)), 0, row, 1, 1);

		for (int column = 0; column < LSDJ_CHANNEL_COUNT; column++) {
			gtk_grid_attach(GTK_GRID(grid), create_chain_widget(model), 1 + column, row, 1, 1);
		}
	}
	gtk_widget_set_vexpand(grid, TRUE);

	return grid;
}
