SHELL=/bin/sh
CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Iliblsdj -Og -g `pkg-config --cflags gtk+-3.0`
LDFLAGS=-L. -llsdj -pedantic -Wall -Wextra `pkg-config --libs gtk+-3.0`

SRC=$(wildcard *.c)
OBJ=$(SRC:.c=.o)
BIN=$(notdir $(shell pwd))
TESTFILE=test.sav

LIBLSDJ=liblsdj.a
LIBLSDJ_CFLAGS=-src=c11 -Og -g
LIBLSDJ_SRC=$(wildcard liblsdj/liblsdj/*.c)
LIBLSDJ_OBJ=$(LIBLSDJ_SRC:.c=.o)

.PHONY: clean $(TESTFILE)

$(BIN):	$(LIBLSDJ) $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

$(LIBLSDJ): $(LIBLSDJ_OBJ)
	ar rv $(LIBLSDJ) $^

liblsdj/liblsdj/%.o: %.c
	$(CC) $(LIBLSDJ_CFLAGS) $^ -o $@

run:	$(BIN)
	./$(BIN)

test:	$(BIN) $(TESTFILE)
	./$(BIN) $(TESTFILE)

debug:	$(BIN)
	GTK_DEBUG=interactive ./$(BIN)

clean:
	rm -rf $(BIN) $(OBJ) $(LIBLSDJ) $(LIBLSDJ_OBJ)
