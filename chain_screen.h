#ifndef CHAIN_SCREEN_H
#define CHAIN_SCREEN_H
#include <gtk/gtk.h>
#include "model.h"

GtkWidget *chain_screen_new(model_t *);
void chain_screen_update(GtkWidget *, lsdj_chain_t *, size_t);

#endif
