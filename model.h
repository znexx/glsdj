#ifndef MODEL_H
#define MODEL_H

#include <gtk/gtk.h>
#include <liblsdj/song.h>

typedef enum {
	UPDATE_SONG_SCREEN,
	UPDATE_CHAIN_SCREEN,	// channel, chain_number
	UPDATE_PHRASE_SCREEN,	// channel, phrase_number
} command_type_t;

typedef struct command_t command_t;

typedef struct model_t model_t;

GtkWidget *get_song_screen(model_t *model);
GtkWidget *get_chain_screen(model_t *model);
GtkWidget *get_phrase_screen(model_t *model);

void set_model_screens(model_t *model, GtkWidget *song_screen, GtkWidget *chain_screen, GtkWidget *phrase_screen);

model_t *create_model(lsdj_song_t *song);
void set_song_screen(model_t *model, GtkWidget *song_screen);
void set_chain_screen(model_t *model, GtkWidget *chain_screen);
void set_phrase_screen(model_t *model, GtkWidget *phrase_screen);
void do_command(model_t *model, command_type_t command_type, int arg1, int arg2, int arg3);
void undo_command(model_t *model);
void redo_command(model_t *model);

#endif
