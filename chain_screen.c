#include <gtk/gtk.h>
#include "liblsdj/chain.h"
#include "liblsdj/channel.h"
#include "chain_screen.h"
#include "print_byte.h"
#include "row_number_widget.h"

static void chain_row_number_button_callback(GtkWidget *widget, gpointer data) {
	data = data;
	if(GTK_IS_BIN(widget)) {
		GtkWidget *child = gtk_bin_get_child(GTK_BIN(widget));
		gint64 chain_id = g_ascii_strtoll(gtk_label_get_text(GTK_LABEL(child)), NULL, 16);
		g_print("%li\n", chain_id);
	}
}

static void phrase_box_callback(GtkWidget *widget, GdkEvent *event, gpointer data) {
	event = event;
	model_t *model = (model_t*) data;
	if (GTK_IS_ENTRY(widget)) {
		gint64 phrase_id = g_ascii_strtoll(gtk_entry_get_text(GTK_ENTRY(widget)), NULL, 16);

		GtkWidget *chain_row_widget = gtk_widget_get_parent(widget);

		GValue gvalue_column = G_VALUE_INIT;
		g_value_init(&gvalue_column, G_TYPE_INT);
		gtk_container_child_get_property(GTK_CONTAINER(get_chain_screen(model)), chain_row_widget, "left-attach", &gvalue_column);
		gint column = g_value_get_int(&gvalue_column);

		do_command(model, UPDATE_PHRASE_SCREEN, phrase_id, column-1, 0);
	}
}

static void transposition_box_callback(GtkWidget *widget, GdkEvent *event, gpointer data) {
	event = event;
	if (GTK_IS_ENTRY(widget)) {
		gint64 chain_id = g_ascii_strtoll(gtk_entry_get_text(GTK_ENTRY(widget)), NULL, 16);
		g_print("%02zx\n", chain_id);
		g_print("%p\n", data);
	}
}

void chain_screen_update(GtkWidget *chain_grid, lsdj_chain_t *chain, size_t channel) {
	gchar phrase[32];
	gchar transposition[32];
	for (size_t row = 0; row < LSDJ_CHAIN_LENGTH; row++) {
		snprint_byte(phrase, sizeof(phrase), chain->phrases[row]);
		snprint_byte(transposition, sizeof(transposition), chain->transpositions[row]);

		GtkWidget *chain_row = gtk_grid_get_child_at(GTK_GRID(chain_grid), channel + 1, row);
		GList *children = gtk_container_get_children(GTK_CONTAINER(chain_row));
		GtkWidget *phrase_box = g_list_first(children)->data;
		GtkWidget *transposition_box = g_list_last(children)->data;

		gtk_entry_set_text(GTK_ENTRY(phrase_box), phrase);
		gtk_entry_set_text(GTK_ENTRY(transposition_box), transposition);
		g_list_free(children);
	}
}

GtkWidget *create_chain_row(model_t *model) {
	GtkWidget *chain_row = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_vexpand(chain_row, TRUE);

	GtkWidget *phrase_box = gtk_entry_new();
	gtk_entry_set_max_width_chars(GTK_ENTRY(phrase_box), 2);
	g_signal_connect(phrase_box, "focus-in-event", G_CALLBACK(phrase_box_callback), model);

	GtkWidget *transposition_box = gtk_entry_new();
	gtk_entry_set_max_width_chars(GTK_ENTRY(transposition_box), 2);
	g_signal_connect(transposition_box, "focus-in-event", G_CALLBACK(transposition_box_callback), model);

	gtk_container_add(GTK_CONTAINER(chain_row), phrase_box);
	gtk_container_add(GTK_CONTAINER(chain_row), transposition_box);
	return chain_row;
}

GtkWidget *chain_screen_new(model_t *model) {
	GtkWidget *grid = gtk_grid_new();

	gtk_grid_set_column_homogeneous(grid, TRUE);

	for (int row = 0; row < LSDJ_CHAIN_LENGTH; row++) {
		gtk_grid_attach(GTK_GRID(grid), create_row_number_widget(model, row, G_CALLBACK(&chain_row_number_button_callback)), 0, row, 1, 1);

		for (int column = 0; column < LSDJ_CHANNEL_COUNT; column++) {
			gtk_grid_attach(GTK_GRID(grid), create_chain_row(model), 1 + column, row, 1, 1);
		}
	}
	gtk_widget_set_vexpand(grid, TRUE);
	return grid;
}
