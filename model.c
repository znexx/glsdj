#include "model.h"
#include "song_screen.h"
#include "chain_screen.h"
#include "phrase_screen.h"
#include <liblsdj/song.h>

struct command_t {
	command_type_t type;
	int arg1;
	int arg2;
	int arg3;

	int old_arg1;
	int old_arg2;
	int old_arg3;

	command_t *previous;
	command_t *next;
};

struct model_t {
	GtkWidget *song_screen;
	GtkWidget *chain_screen;
	GtkWidget *phrase_screen;
	lsdj_song_t *song;

	int chains[4];
	int phrases[4];

	command_t *prev_command;
	command_t *next_command;
};

GtkWidget *get_song_screen(model_t *model) {
	return model->song_screen;
}

GtkWidget *get_chain_screen(model_t *model) {
	return model->chain_screen;
}

GtkWidget *get_phrase_screen(model_t *model) {
	return model->phrase_screen;
}

void set_model_screens(model_t *model, GtkWidget *song_screen, GtkWidget *chain_screen, GtkWidget *phrase_screen) {
	model->song_screen = song_screen;
	model->chain_screen = chain_screen;
	model->phrase_screen = phrase_screen;
}

model_t *create_model(lsdj_song_t *song) {
	model_t *model = calloc(1, sizeof(model_t));
	model->song = song;
	return model;
}

void print_command(command_t *command) {
	switch(command->type) {
		case UPDATE_SONG_SCREEN:
			g_print("Updating song screen\n");
			break;
		case UPDATE_CHAIN_SCREEN:
			g_print("Opening chain %02x on channel %d\n", command->arg1, command->arg2);
			break;
		case UPDATE_PHRASE_SCREEN:
			g_print("Opening phrase %02x on channel %d\n", command->arg1, command->arg2);
			break;
	}
}

void unperform_command(model_t *model, command_t *command) {
	int channel = command->old_arg2;
	switch(command->type) {
		case UPDATE_SONG_SCREEN:
			break;
		case UPDATE_CHAIN_SCREEN:
			model->chains[channel] = command->old_arg1;
			chain_screen_update(model->chain_screen,
				lsdj_song_get_chain(model->song, command->old_arg1),
				channel);
			break;
		case UPDATE_PHRASE_SCREEN:
			model->chains[channel] = command->old_arg1;
			phrase_screen_update(model->phrase_screen,
				lsdj_song_get_phrase(model->song, command->old_arg1),
				channel);
			break;
	}
}

void perform_command(model_t *model, command_t *command) {
	int channel = command->arg2;
	switch(command->type) {
		case UPDATE_SONG_SCREEN:
			song_screen_update(model->song_screen,
				model->song);
			break;
		case UPDATE_CHAIN_SCREEN:
			command->old_arg1 = model->chains[channel];
			command->old_arg2 = channel;
			model->chains[channel] = command->arg1;

			chain_screen_update(model->chain_screen,
				lsdj_song_get_chain(model->song, command->arg1),
				channel);
			break;
		case UPDATE_PHRASE_SCREEN:
			command->old_arg1 = model->phrases[channel];
			command->old_arg2 = channel;
			model->phrases[channel] = command->arg1;

			phrase_screen_update(model->phrase_screen,
				lsdj_song_get_phrase(model->song, command->arg1),
				channel);
			break;
	}
}

void print_command_history(model_t *model) {
	g_print("\nCommand history:\n");
	command_t *command = model->prev_command;
	while(command) {
		print_command(command);
		command = command->previous;
	}
}

void do_command(model_t *model, command_type_t command_type, int arg1, int arg2, int arg3) {
	command_t *new_command = calloc(1, sizeof(command_t));
	new_command->type = command_type;
	new_command->arg1 = arg1;
	new_command->arg2 = arg2;
	new_command->arg3 = arg3;

	perform_command(model, new_command);

	command_t *command = model->next_command;
	while(command != NULL) {
		command_t *next = command->next;
		free(command);
		command = next;
	}

	if(model->prev_command) {
		model->prev_command->next = new_command;
		new_command->previous = model->prev_command;
	}

	model->next_command = NULL;
	model->prev_command = new_command;

	print_command_history(model);
}

void undo_command(model_t *model) {
	if(model->prev_command != NULL) {
		unperform_command(model, model->prev_command);

		model->next_command = model->prev_command;
		model->prev_command = model->prev_command->previous;
	}
	print_command_history(model);
}

void redo_command(model_t *model) {
	if(model->next_command != NULL) {
		perform_command(model, model->next_command);

		model->prev_command = model->next_command;
		model->next_command = model->next_command->next;
	}
	print_command_history(model);
}
