#ifndef PRINT_BYTE_H
#define PRINT_BYTE_H

void print_byte(unsigned char byte);
void snprint_byte(gchar *, size_t, unsigned char);

#endif
