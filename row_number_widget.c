#include <gtk/gtk.h>
#include "model.h"
#include "row_number_widget.h"

GtkWidget *create_row_number_widget(model_t *model, int row, GCallback *callback) {
		gchar row_number[4];
		g_snprintf(row_number, 3, "%02x", row);
		GtkWidget *chain_row_number = gtk_button_new_with_label(row_number);
		g_signal_connect(chain_row_number, "clicked", G_CALLBACK(callback), model);
		return chain_row_number;
}

