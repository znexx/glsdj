#include <gtk/gtk.h>
#include <liblsdj/project.h>
#include <liblsdj/error.h>
#include <liblsdj/sav.h>
#include <liblsdj/song.h>
#include "song_screen.h"
#include "chain_screen.h"
#include "phrase_screen.h"
#include "model.h"

model_t *model;

void open_button_callback(void) {
	g_print("open!\n");
}

void save_button_callback(void) {
	g_print("save!\n");
}

void undo_button_callback(void) {
	undo_command(model);
}

void redo_button_callback(void) {
	redo_command(model);
}

static void create_main_window(GtkApplication *app) {
	lsdj_error_t *error;

	// open sav file

	lsdj_sav_t *sav = lsdj_sav_read_from_file("test.sav", &error);
	if (error) {
		printf("Error: %s\n", lsdj_error_get_c_str(error));
		g_application_quit(G_APPLICATION(app));
		return;
	}
	lsdj_project_t *project = lsdj_sav_get_project(sav, 0x1a);
	lsdj_song_t *song = lsdj_project_get_song(project);


	char title[LSDJ_PROJECT_NAME_LENGTH];
	lsdj_project_get_name(project, title, LSDJ_PROJECT_NAME_LENGTH);

	model = create_model(song);

	// create window

	GtkWidget *window = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(window), "gLSDJ ");
	gtk_window_set_default_size(GTK_WINDOW(window), 500, 300);

	GtkCssProvider *provider = gtk_css_provider_new();
	GdkDisplay *display = gdk_display_get_default();
	GdkScreen *screen = gdk_display_get_default_screen(display);
	gtk_style_context_add_provider_for_screen(screen, GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_USER);
	GError *gerror = NULL;
	gtk_css_provider_load_from_file(provider, g_file_new_for_path("style.css"), &gerror);

	if(gerror) {
		g_print("Unable to read file: %s\n", gerror->message);
		g_error_free(gerror);
		return;
	}

	g_object_unref(provider);

	// header bar

	GtkWidget *headerbar = gtk_header_bar_new();
	gtk_header_bar_set_title(GTK_HEADER_BAR(headerbar), title);
	gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(headerbar), 0);

	GtkWidget *toolbar = gtk_toolbar_new();

	GtkToolItem *open_button = gtk_tool_button_new(
			gtk_image_new_from_icon_name("document-open", 128),
			NULL);
	g_signal_connect(open_button, "clicked", open_button_callback, NULL);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), open_button, 0);

	GtkToolItem *save_button = gtk_tool_button_new(
			gtk_image_new_from_icon_name("document-save", 128),
			NULL);
	g_signal_connect(save_button, "clicked", save_button_callback, NULL);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), save_button, 1);

	GtkToolItem *undo_button = gtk_tool_button_new(
			gtk_image_new_from_icon_name("edit-undo", 128),
			NULL);
	g_signal_connect(undo_button, "clicked", undo_button_callback, NULL);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), undo_button, 2);

	GtkToolItem *redo_button = gtk_tool_button_new(
			gtk_image_new_from_icon_name("edit-redo", 128),
			NULL);
	g_signal_connect(redo_button, "clicked", redo_button_callback, NULL);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), redo_button, 3);

	gtk_header_bar_pack_start(GTK_HEADER_BAR(headerbar), GTK_WIDGET(toolbar));

	gtk_window_set_titlebar(GTK_WINDOW(window), headerbar);
	gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(headerbar), 1);

	// main layout

	GtkWidget *main_split = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_container_add(GTK_CONTAINER(window), main_split);

	GtkWidget *left_split = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(main_split), left_split);

	GtkWidget *song_screen = song_screen_new(model);
	GtkWidget *chain_screen = chain_screen_new(model);
	GtkWidget *phrase_screen = phrase_screen_new(model);

	set_model_screens(model, song_screen, chain_screen, phrase_screen);

	gtk_container_add(GTK_CONTAINER(main_split), phrase_screen);
	gtk_container_add(GTK_CONTAINER(left_split), song_screen);
	gtk_container_add(GTK_CONTAINER(left_split), chain_screen);

	gtk_widget_show_all(window);

	do_command(model, UPDATE_SONG_SCREEN, 0, 0, 0);
}

static void activate(GtkApplication *app, gpointer user_data) {
	user_data = user_data;
	create_main_window(app);
}

int main (int argc, char **argv) {
	GtkApplication *app = gtk_application_new("at.kattm.lsdj", G_APPLICATION_FLAGS_NONE);

	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	//g_application_add_main_option_entries(G_APPLICATION(app), );
	
	int status = g_application_run(G_APPLICATION(app), argc, argv);
	free(model);

	g_object_unref(app);

	return status;
}
