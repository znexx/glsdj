#include <gtk/gtk.h>
#include "phrase_screen.h"
#include "row_number_widget.h"

static void phrase_row_number_button_callback(GtkWidget *widget, gpointer data) {
	data = data;
	if(GTK_IS_BIN(widget)) {
		GtkWidget *child = gtk_bin_get_child(GTK_BIN(widget));
		gint64 chain_id = g_ascii_strtoll(gtk_label_get_text(GTK_LABEL(child)), NULL, 16);
		g_print("%li\n", chain_id);
	}
}

static void note_button_callback(GtkWidget *widget, gpointer data) {
	data = data;
	if(GTK_IS_BIN(widget)) {
		GtkWidget *child = gtk_bin_get_child(GTK_BIN(widget));
		gint64 chain_id = g_ascii_strtoll(gtk_label_get_text(GTK_LABEL(child)), NULL, 16);
		g_print("%li\n", chain_id);
	}
}

void phrase_screen_update(GtkWidget *phrase_grid, lsdj_phrase_t *chain, size_t channel) {
	g_print("Updating phrase screen!\n");
}

GtkWidget *create_phrase_row(model_t *model) {
	GtkWidget *phrase_row = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_vexpand(phrase_row, TRUE);

	GtkWidget *note_box = gtk_entry_new();
	GtkWidget *instrument_box = gtk_entry_new();
	GtkWidget *command_box = gtk_entry_new();
	GtkWidget *command_value_box = gtk_entry_new();

	gtk_container_add(GTK_CONTAINER(phrase_row), note_box);
	gtk_container_add(GTK_CONTAINER(phrase_row), instrument_box);
	gtk_container_add(GTK_CONTAINER(phrase_row), command_box);
	gtk_container_add(GTK_CONTAINER(phrase_row), command_value_box);
	return phrase_row;
}

GtkWidget *phrase_screen_new(model_t * model) {
	GtkWidget *grid = gtk_grid_new();
	for (int row = 0; row < LSDJ_PHRASE_LENGTH; row++) {
		gtk_grid_attach(GTK_GRID(grid), create_row_number_widget(model, row, G_CALLBACK(&phrase_row_number_button_callback)), 0, row, 1, 1);

		for (int column = 0; column < LSDJ_CHANNEL_COUNT; column++) {
			gtk_grid_attach(GTK_GRID(grid), create_phrase_row(model), 1 + column, row, 1, 1);
		}
	}

	gtk_widget_set_vexpand(grid, TRUE);
	return grid;
}
