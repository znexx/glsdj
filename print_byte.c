#include <stdio.h>
#include <gtk/gtk.h>
#include "print_byte.h"

void print_byte(unsigned char byte) {
	if(byte == 0xff) {
		g_print("   ");
	} else {
		g_print(" %02x", byte);
	}
}

void snprint_byte(gchar *string, size_t n, unsigned char byte) {
	if(byte != 0xff) {
		g_snprintf(string, n, "%02x", byte);
	} else {
		g_snprintf(string, n, "--");
	}
}
