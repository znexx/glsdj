#ifndef ROW_NUMBER_WIDGET_H
#define ROW_NUMBER_WIDGET_H

#include <gtk/gtk.h>
#include "model.h"

GtkWidget *create_row_number_widget(model_t *model, int row, GCallback *callback);

#endif
