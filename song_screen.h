#ifndef SONG_SCREEN_H
#define SONG_SCREEN_H
#include <gtk/gtk.h>
#include "model.h"

GtkWidget *song_screen_new(model_t *);
void song_screen_update(GtkWidget *, lsdj_song_t *);

#endif
