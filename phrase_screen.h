#ifndef PHRASE_SCREEN_H
#define PHRASE_SCREEN_H
#include <gtk/gtk.h>
#include "model.h"

GtkWidget *phrase_screen_new(model_t *);
void phrase_screen_update(GtkWidget *, lsdj_phrase_t *, size_t);

#endif
